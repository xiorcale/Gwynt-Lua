--require "../card"

local contentsManipulation = {}

function contentsManipulation:new(object)
  object = object or {}

  setmetatable(object, self) -- use contents as a prototype for the newly created object
  self.__index = self

  return object
end


function contentsManipulation:initGame()
  self.zones = {{}, {}, {}, {}, {}}
  self.adZones = {{}, {}, {}, {}, {}}

  self.zonesScores = {0, 0, 0}
  self.adZonesScores = {0, 0, 0}

  self.score = {0}
  self.adScore = {0}

  self.selectedCard = nil
end

function contentsManipulation:initHand()
  self.hand = {}
  local set = {} 
  local index = math.random(#factionsDeck[factionChoosed])

  for i=1, #placeholders:hand(), 1 do
    -- init the card in the hand, and make sure we don't draw 2 times the same card
    while set[index] ~= nil do
      index = math.random(#factionsDeck[factionChoosed])
    end

    set[index] = index
    self.hand[i] = factionsDeck[factionChoosed][index]
  end
  
  self.currentHandSize = 8
end


function contentsManipulation:selectCard(x, y)
  -- Try to select a card in the hand
  for i=1, #placeholders:hand(), 1 do
    if self.hand[i] ~= nil then
      if x > SCALE*placeholders:hand()[i].x and x < SCALE*placeholders:hand()[i].x + SCALE*placeholders:hand()[i].width and
      y > SCALE*placeholders:hand()[i].y and y < SCALE*placeholders:hand()[i].y + SCALE*placeholders:hand()[i].height
      then
        self.hand[i].isSelected = true
        self.selectedCard = self.hand[i]
        self.selectedCard.index = i
        break
      end
    end
  end
end


function contentsManipulation:moveCard(x, y)
  -- try to move a card, if any is selected
  local peripheralZone = placeholders:peripheralZones()[self.selectedCard.zone]
  local zone = placeholders:zones()[self.selectedCard.zone]

  -- make sure the user click inside the right zone
  if x > SCALE*peripheralZone.x and x < SCALE*peripheralZone.x + SCALE*peripheralZone.width and
  y > SCALE*peripheralZone.y and y < SCALE*peripheralZone.y + SCALE*peripheralZone.height
  then

    -- find the closest placeholder and make sure it's empty
    for i=1, #zone, 1 do
      if x > SCALE*zone[i].x - SCALE*40 and x < SCALE*zone[i].x + SCALE*(zone[i].width+40) and
      y > SCALE*zone[i].y - SCALE*10 and y < SCALE*zone[i].y + SCALE*(zone[i].height+10) 
      then
        if self.selectedCard.zone == WEATHER or self.zones[self.selectedCard.zone][i] == nil then

          -- move the card in the proper table and remove it from the hand
          self.zones[self.selectedCard.zone][i] = self.selectedCard
          self.hand[self.selectedCard.index] = nil
          self.currentHandSize = self.currentHandSize -1

          -- Update the weather conditions and the scores
          if self.selectedCard.zone == WEATHER then -- sync weather between client/server
            self.adZones[WEATHER][1] = self.zones[WEATHER][1]
          end

          scores:updateZonesScores()
          scores:updateAdZonesScores()
          scores:updateWeather()
          scores:updateBoost()
          scores:updateAdBoost()
          scores:updateScore()

          network:changeTurn(self.selectedCard.zone, i, self.selectedCard.deckIndex, self.zonesScores[MELEE], self.zonesScores[DISTANCE], self.zonesScores[SIEGE], self.score[1])
          break
        end
      end
    end
  end

  self.selectedCard.isSelected = false
  self.selectedCard = nil
end


return contentsManipulation
