local scores = {}


function scores:updateScore()
  contents.score[1] = contents.zonesScores[MELEE] + contents.zonesScores[DISTANCE] + contents.zonesScores[SIEGE]
  contents.adScore[1] = contents.adZonesScores[MELEE] + contents.adZonesScores[DISTANCE] + contents.adZonesScores[SIEGE]

end


function scores:updateZonesScores()
  for j=1, #contents.zonesScores, 1 do
    contents.zonesScores[j] = 0

    for i=1, #placeholders:zones()[j], 1 do
      if contents.zones[j][i] ~= nil then
        contents.zonesScores[j] = contents.zonesScores[j] + contents.zones[j][i].value
      end
    end
  end
end


function scores:updateBoost()
  for i=1, #placeholders:zones()[BOOST], 1 do
    if contents.zones[BOOST][i] ~= nil then
      contents.zonesScores[i] = 2 * contents.zonesScores[i]
    end
  end
end


function scores:updateAdZonesScores()
  for j=1, #contents.adZonesScores, 1 do
    contents.adZonesScores[j] = 0

    for i=1, #placeholders:adZones()[j], 1 do
      if contents.adZones[j][i] ~= nil then
        contents.adZonesScores[j] = contents.adZonesScores[j] + contents.adZones[j][i].value
      end
    end
  end
end


function scores:updateAdBoost()
  for j=1, #placeholders:adZones()[BOOST], 1 do
    if contents.adZones[BOOST][j] ~= nil then
      contents.adZonesScores[j] = 2 * contents.adZonesScores[j]
    end
  end
end


function scores:updateWeather()
  if contents.zones[WEATHER][1] ~= nil then
    contents.zones[WEATHER][1].power()
  end
end


function scores:updateGem()
  if tonumber(contents.score[1]) > tonumber(contents.adScore[1]) then
    if not WIN_ROUND then
      WIN_ROUND = true
      YOUR_TURN = false
    else
      IS_WINNER = true
      gamestate.switch(win)
    end
  elseif tonumber(contents.score[1]) == tonumber(contents.adScore[1]) then
    if not WIN_ROUND and not AD_WIN_ROUND then
      WIN_ROUND = true
      AD_WIN_ROUND = true
    else
      IS_WINNER = true
      gamestate.switch(win)
    end

    if IS_SERVER then
      YOUR_TURN = false
    else
      YOUR_TURN = true
    end
  else
    if not AD_WIN_ROUND then
      AD_WIN_ROUND = true
      YOUR_TURN = true
    else
      gamestate.switch(win)
    end
  end
end

return scores
