local graphics = {}

enlargeHelp = {false, false}

-- Icon images
local TOTALIMG = love.graphics.newImage("assets/totalICON.png")
local WINGEMIMG = love.graphics.newImage("assets/gemICON.png")

-- Sound images
local SOUNDIMGNORMAL = love.graphics.newImage("assets/sound.png")
local SOUNDIMGMUTED = love.graphics.newImage("assets/mute.png")

-- Help Cards 
local ORIGINAL_HELPCARDS = {love.graphics.newImage("assets/Help1_original2.png"), love.graphics.newImage("assets/Help2_original2.png")}
local HELPCARDS = {love.graphics.newImage("assets/Help1.2.png"), love.graphics.newImage("assets/Help2.2.png")}

function graphics:drawHand(hand)
  for i = 1, #placeholders:hand(), 1 do
    if hand[i] ~= nil then
      -- draw card and range's border if selected
      if hand[i].mouseIsHover or hand[i].isSelected then

        zone = board.layers["ZONES"].objects[hand[i].zone]


        love.graphics.setColor(unpack(ORANGE)) -- orange
        love.graphics.rectangle("line", SCALE*zone.x, SCALE*zone.y, SCALE*zone.width, SCALE*zone.height) -- zone border
        love.graphics.rectangle("line", SCALE*placeholders:hand()[i].x - 2.5, SCALE*placeholders:hand()[i].y - 2.5, SCALE*placeholders:hand()[i].width + 5, SCALE*placeholders:hand()[i].height + 5) -- card border

        love.graphics.setColor(unpack(WHITE))
        love.graphics.draw(hand[i].image, SCALE*board.layers["DISPLAY"].objects[1].x, SCALE*board.layers["DISPLAY"].objects[1].y, 0, 2.5*SCALE)

      end

      love.graphics.setColor(unpack(WHITE))
      love.graphics.draw(hand[i].image, SCALE*placeholders:hand()[i].x, SCALE*placeholders:hand()[i].y, 0, SCALE) 
    end
  end
end


function graphics:drawZones(zones, placeholdersZone)
  for j = 1, #placeholdersZone, 1 do
    for i = 1, #placeholders:zones()[j], 1 do
      if zones[j] ~= nil and zones[j][i] ~= nil then
        local placeholder = placeholdersZone[zones[j][i].zone][i]
        love.graphics.draw(zones[j][i].image, SCALE*placeholder.x, SCALE*placeholder.y, 0, SCALE) 
      end
    end
  end
end


function graphics:drawLeader()
  love.graphics.draw(leaderImg[factionChoosed][leaderCardNumber], SCALE*placeholders:score()[10].x,  SCALE*placeholders:score()[10].y, 0, SCALE)
  love.graphics.draw(leaderImg[adFactionChoosed][leaderAdCardNumber], SCALE*placeholders:score()[5].x,  SCALE*placeholders:score()[5].y, 0, SCALE)
end 


function graphics:drawHelpCards()
  for i=1, #enlargeHelp, 1 do
    if enlargeHelp[i] then
      love.graphics.draw(ORIGINAL_HELPCARDS[i], SCALE*placeholders:adZones()[3][2].x, SCALE*SCALE*placeholders:adZones()[3][2].y, 0, 0.8)
    end

    love.graphics.draw(HELPCARDS[i], SCALE*placeholders:help()[1].x, SCALE*placeholders:help()[i].y, 0, SCALE)
  end
end


function graphics:drawZonesScores(zonesScores, score, adZonesScores, adScore)
  for i = 1, #placeholders:zonesScores(), 1 do
    love.graphics.print(zonesScores[i], SCALE*placeholders:zonesScores()[i].x, SCALE*placeholders:zonesScores()[i].y, 0, 2)
    love.graphics.print(tonumber(adZonesScores[i]), SCALE*placeholders:adZonesScores()[i].x, SCALE*placeholders:adZonesScores()[i].y, 0, 2)
  end

  love.graphics.print(score[1], SCALE*placeholders:score()[9].x, SCALE*placeholders:score()[9].y, 0, 3)
  love.graphics.print(adScore[1], SCALE*placeholders:score()[4].x, SCALE*placeholders:score()[4].y, 0, 3)

  --add icon for total troop forces
  love.graphics.draw(TOTALIMG, SCALE*placeholders:score()[8].x, SCALE*placeholders:score()[8].y, 0, 0.8)
  love.graphics.draw(TOTALIMG, SCALE*placeholders:score()[3].x, SCALE*placeholders:score()[3].y, 0, 0.8)
end


function graphics:drawPass()
  love.graphics.setColor(unpack(ORANGE))
  love.graphics.arc("line", "open", SCALE*300, SCALE*1075, 25, 0, angle, 50)

  if AD_PASS then
    love.graphics.arc("line", "open", SCALE*300, SCALE*575, 25, 0, 6.3, 50)
  end
end


function graphics:drawGem()
  love.graphics.setColor(unpack(WHITE))
  if WIN_ROUND then
    love.graphics.draw(WINGEMIMG, SCALE*placeholders:score()[6].x, SCALE*placeholders:score()[6].y, 0, 0.2)
  end

  if AD_WIN_ROUND then 
    love.graphics.draw(WINGEMIMG, SCALE*placeholders:score()[1].x, SCALE*placeholders:score()[1].y, 0, 0.2)
  end
end


function graphics:drawSoundImg()
  love.graphics.draw(SOUNDIMGNORMAL, SCALE*placeholders:sound()[1].x, SCALE*placeholders:sound()[1].y, 0, 0.2)
end
function graphics:drawMuteImg()
  love.graphics.draw(SOUNDIMGMUTED, SCALE*placeholders:sound()[1].x, SCALE*placeholders:sound()[1].y, 0, 0.2)
end

return graphics
