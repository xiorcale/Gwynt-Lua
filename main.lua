-- State machine
gamestate = require "lib.hump.gamestate"

menu = require "states.menu"
settings = require "states.settings"
connect = require "states.connect"
game = require "states.game"
pause = require "states.pause"
factionSelection = require "states.factionSelection"
waitScreen = require "states.waitScreen"
win = require "states.win"

-- content
require "decks"
contentsManipulation = require "contentsManipulation"
contents = contentsManipulation:new() -- global


-- network communication
require "enet"
network = require "network"

-- gameboard informations (global variable)
sti = require "lib/sti"

board = sti("assets/GameBoard.lua")
board:resize(3300,2000)

placeholders = require "placeholders"
scores = require "scores"

require "config"


function love.load() 
  love.keyboard.setKeyRepeat(true)
  love.window.setIcon(love.image.newImageData("assets/logo.png"))
	gamestate.registerEvents()
	gamestate.switch(menu)
end
