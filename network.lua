local network = {}

local server


------------------------
--  Public Functions  --
------------------------

function network:initNetworkCommunication()
  if IS_SERVER then
    serverConnection()
  else
    clientConnection()
  end

  sendFactionInfo()
  return initFactionAd()
end


function network:updateEvent()
  local event = host:service()
  if event ~= nil then
    if event.type == "receive" then
      YOUR_TURN = true

      if event.data == "pass" then
        AD_PASS = true
       
        if PASS  then
          scores:updateGem()
          contents:initGame()
          PASS = false
          AD_PASS = false
          angle = 0
        end
      else
        data = eventData(event)

        contents.adZones[data.zone][data.index] = factionsDeck[adFactionChoosed][data.deckIndex]

        if data.zone == WEATHER then
          contents.zones[WEATHER] = contents.adZones[WEATHER] -- since it's the same placeholder, it has to be synchronized
        end

        scores:updateZonesScores()
        scores:updateWeather()
        scores:updateBoost()
        scores:updateAdBoost()
        scores:updateScore()

        contents.adZonesScores[MELEE] = data.adZonesScores[MELEE]
        contents.adZonesScores[DISTANCE] = data.adZonesScores[DISTANCE]
        contents.adZonesScores[SIEGE] = data.adZonesScores[SIEGE]

        contents.adScore[1] = data.adScore
      end
    end
  end
end

function network:changeTurn(zone, index, deckIndex, scoreMelee, scoreDistance, scoreSiege, score)
  YOUR_TURN = false

  deckIndex = addLeadingZero(deckIndex)

  scoreMelee = addLeadingZero(scoreMelee)
  scoreDistance = addLeadingZero(scoreDistance)
  scoreSiege = addLeadingZero(scoreSiege)

  host:get_peer(1):send(zone .. index .. deckIndex .. scoreMelee .. scoreDistance .. scoreSiege .. score) 
end


function network:passTurn()
  love.keyboard.setKeyRepeat(false)
  host:get_peer(1):send("pass")
  host:flush()
  
  if AD_PASS then
    scores:updateGem()
    contents:initGame()
    PASS = false
    AD_PASS = false
    angle = 0
  else
    PASS = true
  end
end

-------------------------
--  Private Functions  --
-------------------------

function serverConnection()
  host = enet.host_create(IP .. ":6789")

  local done = false
  while not done do
    local event = host:service(100)
    if event then
      if event.type == "receive" then
        done = true
      end
    end
  end
end


function clientConnection()
  host = enet.host_create()
  server =  host:connect(IP .. ":6789")

  local done = false
  while not done do
    local event = host:service(100)
    if event then
      event.peer:send("connected") 
      done =  true
    end
  end
end


function sendFactionInfo()
  host:get_peer(1):send(factionChoosed .. leaderCardNumber)
end


function initFactionAd()
  local event
  while event == nil do
    event = host:service()
  end

  if event.type == "receive" then
    return {tonumber(string.sub(event.data, 1, 1)), tonumber(string.sub(event.data, 2))}
  end
end


function eventData(event)
  return {
    zone = tonumber(string.sub(event.data, 1, 1)), 
    index = tonumber(string.sub(event.data, 2, 2)),
    deckIndex = tonumber(string.sub(event.data, 3, 4)),
    adZonesScores = {string.sub(event.data, 5, 6), string.sub(event.data, 7, 8), string.sub(event.data, 9, 10)},
    adScore = string.sub(event.data, 11)
  }
end

return network
