require "../config"

local factionSelection = {}
local suit = require "../lib/suit"


function factionSelection:init()
  suit.theme.color.normal = {fg = {0,0,0}, bg = {236, 240, 241, 106}}
  suit.theme.color.hovered = {fg = {0,0,0}, bg = {236, 240, 241, 136}}
  suit.theme.color.active = {fg = {255,255,255}, bg = {0,0,0,136}}

  love.graphics.setBackgroundColor(0,0,0)
  
  submitted = false
end


function factionSelection:draw()
  suit.draw()
end


function factionSelection:update(dt) 
  if suit.Button("Wild Hunt", X_POS, Y_CENTER - 75, 300, 30).hit then
    factionChoosed = 1
    gamestate.switch(waitScreen)

  end

  if suit.Button("Scoia'Tael", X_POS, Y_CENTER - 25, 300, 30).hit then
    factionChoosed = 2
    gamestate.switch(waitScreen)
  end

  if suit.Button("Nilfgaard", X_POS, Y_CENTER + 25, 300, 30).hit then
    factionChoosed = 3
    gamestate.switch(waitScreen)
  end

  if suit.Button("Northern Realms", X_POS, Y_CENTER + 75, 300, 30).hit then
    factionChoosed = 4
    gamestate.switch(waitScreen)
  end
end


function factionSelection:keypressed(key)
  suit.keypressed(key)
end


return factionSelection
