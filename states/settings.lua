local settings = {}

local suit = require '../lib/suit'


function settings:init()
  checkbox1024x768 = {checked = true}
  love.graphics.setBackgroundColor(255,255,255)
end


function settings:draw()
  suit.draw()
end


function settings:update(dt)
  suit.Checkbox(checkbox1024x768, 100, 100, 50, 50)
  suit.Label("1024x768", 150, 120, 100, 20)
end


return settings
