require "../config"

local connect = {}
local suit = require "../lib/suit"


function connect:init()
  suit.theme.color.normal = {fg = {0,0,0}, bg = {236, 240, 241, 106}}
  suit.theme.color.hovered = {fg = {0,0,0}, bg = {236, 240, 241, 136}}
  suit.theme.color.active = {fg = {255,255,255}, bg = {0,0,0,136}}

  love.graphics.setBackgroundColor(0,0,0)

  ipAddr = {text = "127.0.0.1"}
  submitted = false
end


function connect:draw()
  suit.draw()
end


function connect:update(dt) 
  if submitted == false then
    if suit.Button("Server", X_POS, Y_CENTER-20, 300, 30).hit then
      IS_SERVER = true
      YOUR_TURN = true
      submitted = true
    end

    if suit.Button("Client", X_POS, Y_CENTER+50, 300, 30).hit then
      submitted = true
    end
  else
    if suit.Button("Ok", X_POS, Y_CENTER + 50, 300, 30).hit or suit.Input(ipAddr, X_POS, Y_CENTER-20, 300, 30).submitted then
      IP = ipAddr.text
      gamestate.switch(factionSelection)
    end
  end
end


function connect:textinput(t)
  suit.textinput(t)
end


function connect:keypressed(key)
  suit.keypressed(key)
end


return connect
