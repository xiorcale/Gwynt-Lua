require "../card"

local game = {}

local graphics = require "../graphics"


function game:init()
  love.window.setMode(1360,825)

  --seeding for true random numbers
  math.randomseed(os.time())

  contents:initGame()
  contents:initHand()

-- Init leader 
  leaderCardNumber = love.math.random(#leaderImg[factionChoosed])
  adFactionInfo = network:initNetworkCommunication()

  adFactionChoosed = adFactionInfo[1]
  leaderAdCardNumber = adFactionInfo[2]

--Music purpose
  music = love.audio.newSource("assets/GwyntSoundtrack.mp3")
  music:setLooping(true)
  music:play()

  -- design
  love.graphics.setLineWidth(5)
end


function game:update(dt)
  board:update(dt)
  network:updateEvent()
end


function game:draw()
  -- draw the board
  love.graphics.setColor(unpack(WHITE))
  board:draw(0, 0, SCALE, SCALE)

  graphics:drawHand(contents.hand)
  graphics:drawZones(contents.zones, placeholders:zones())
  graphics:drawZones(contents.adZones, placeholders:adZones())
  graphics:drawZonesScores(contents.zonesScores, contents.score, contents.adZonesScores, contents.adScore)
  graphics:drawLeader()  
  graphics:drawHelpCards()
  graphics:drawPass()
  graphics:drawGem()
  
  if music:isPlaying() then
    graphics:drawSoundImg()
  else
    graphics:drawMuteImg()
  end
end


function game:mousepressed(x, y, button)
  if YOUR_TURN and not PASS or AD_PASS then
    if contents.selectedCard ~= nil then
      contents:moveCard(x, y)
      if contents.currentHandSize == 0 then
        network:passTurn()
      end
    else
      contents:selectCard(x, y)
    end
  end
  
  if x > SCALE*placeholders:sound()[1].x and x < SCALE*placeholders:sound()[1].x + SCALE*placeholders:sound()[1].width and
  y > SCALE*placeholders:sound()[1].y and y < SCALE*placeholders:sound()[1].y + SCALE*placeholders:sound()[1].height then
    if music:isPlaying() then
      music:pause()
    else
      music:resume()
    end
  end
end


function game:mousemoved(x, y, dx, dy, istouch)
  -- mouse hover card animation
  for i = 1, #placeholders:hand(), 1 do
    if contents.hand[i] ~= nil then
      if x > SCALE*placeholders:hand()[i].x and x < SCALE*placeholders:hand()[i].x + SCALE*placeholders:hand()[i].width and
      y > SCALE*placeholders:hand()[i].y and y < SCALE*placeholders:hand()[i].y + SCALE*placeholders:hand()[i].height then
        contents.hand[i].mouseIsHover = true
      else
        contents.hand[i].mouseIsHover = false
      end
    end
  end

  for i = 1, #placeholders:help(), 1 do 
    if x >  SCALE*placeholders:help()[i].x and  x <  SCALE*placeholders:help()[i].x + SCALE*placeholders:help()[i].width and
    y >  SCALE*placeholders:help()[i].y and  y <  SCALE*placeholders:help()[i].y + SCALE*placeholders:help()[i].height then
      enlargeHelp[i] = true
    else
      enlargeHelp[i] = false
    end
  end
end


function game:keypressed(key, scancode, isrepeat)
  if key == 'escape' then
    gamestate.switch(pause)
  elseif key == 'space' and angle < 6.2 then
    love.keyboard.setKeyRepeat(true)
    angle = angle + 0.15
  end

  if angle > 6.2 then 
    network:passTurn()
  end
end


function game:keyreleased(key)
  if key == "space" and angle < 6.2 then
    angle = 0
  end
end


return game
