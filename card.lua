local card = {}

function card:new(object)
  object = object or {}
  
  setmetatable(object, self)
  self.__index = self
  
  return object
end

return card
